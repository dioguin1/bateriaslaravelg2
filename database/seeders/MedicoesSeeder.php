<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('medicoes')->insert(
            [
                [
                    'pilha_bateria'     => 'Pilha Alcalina Duracel AA',
                    'tensao_nominal'     => 1.5,
                    'capacidade_corrente'     => 2800,
                    'tensao_sem_carga'     => 1.304,
                    'tensao_com_carga'     => 1.286,
                    'resistencia_carga'     => 23.7,
                ],
                [
                    'pilha_bateria'     => 'Bateria 12V 7Ah',
                    'tensao_nominal'     => 12,
                    'capacidade_corrente'     => 7000,
                    'tensao_sem_carga'     => 10.48,
                    'tensao_com_carga'     => 10.32,
                    'resistencia_carga'     => 23.7,
                ],
                [
                    'pilha_bateria'     => 'Bateria recarregável Elgin 9V',
                    'tensao_nominal'     => 9,
                    'capacidade_corrente'     => 250,
                    'tensao_sem_carga'     => 8.53,
                    'tensao_com_carga'     => 6.23,
                    'resistencia_carga'     => 23.7,
                ],
                [
                    'pilha_bateria'     => 'Golite',
                    'tensao_nominal'     => 9,
                    'capacidade_corrente'     => 500,
                    'tensao_sem_carga'     => 5.829,
                    'tensao_com_carga'     => 2.547,
                    'resistencia_carga'     => 24,
                ],
                [
                    'pilha_bateria'     => 'J.Y.X',
                    'tensao_nominal'     => 3.7,
                    'capacidade_corrente'     => 1500,
                    'tensao_sem_carga'     => 2.826,
                    'tensao_com_carga'     => 2.741,
                    'resistencia_carga'     => 24,
                ],
                [
                    'pilha_bateria'     => 'Luatek',
                    'tensao_nominal'     => 3.7,
                    'capacidade_corrente'     => 1500,
                    'tensao_sem_carga'     => 2.639,
                    'tensao_com_carga'     => 2.552,
                    'resistencia_carga'     => 24,
                ],
                [
                    'pilha_bateria'     => 'Panasonic',
                    'tensao_nominal'     => 1.5,
                    'capacidade_corrente'     => 2100,
                    'tensao_sem_carga'     => 1.362,
                    'tensao_com_carga'     => 1.324,
                    'resistencia_carga'     => 24,
                ],
                [
                    'pilha_bateria'     => 'Duracell AAA',
                    'tensao_nominal'     => 1.5,
                    'capacidade_corrente'     => 1000,
                    'tensao_sem_carga'     => 0.994,
                    'tensao_com_carga'     => 0.851,
                    'resistencia_carga'     => 24,
                ],
                [
                    'pilha_bateria'     => 'Phillips AAA',
                    'tensao_nominal'     => 1.5,
                    'capacidade_corrente'     => 1000,
                    'tensao_sem_carga'     => 1.379,
                    'tensao_com_carga'     => 1.331,
                    'resistencia_carga'     => 24,
                ],
                [
                    'pilha_bateria'     => 'Unipower',
                    'tensao_nominal'     => 12,
                    'capacidade_corrente'     => 7,
                    'tensao_sem_carga'     => 10.911,
                    'tensao_com_carga'     => 10.116,
                    'resistencia_carga'     => 24,
                ],
            ]
        );
    }
}
