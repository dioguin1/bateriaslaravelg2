var dados = [ 
    {
        pilha: "Energy elgin",
        tensao_nominal: 9,
        corrente: 0.58,
        E: 7.346,
        V: 2.135,
        R: 24.000,
    },

    {
        pilha: "Golite",
        tensao_nominal: 9,
        corrente: 0.58,
        E: 5.829,
        V: 2.547,
        R: 24.000,
    },

    {
        pilha: "J.Y.X",
        tensao_nominal: 3.7,
        corrente: 1.5,
        E: 2.826,
        V: 2.741,
        R: 24.000,
    },

    {
        pilha:"Luatek",
        tensao_nominal:3.7,
        corrente:1.5,
        E: 2.639,
        V: 2.552,
        R: 24.000,
    },

    {
        pilha: "Pilha Alcalina Duracel AA",
        tensao_nominal: 1.5,
        corrente: 2.8,
        E: 1.362,
        V: 1.324,
        R: 24.000,
    },

    {
        pilha:"Panasonic",
        tensao_nominal:1.5,
        corrente:2.8,
        E: 1.392,
        V: 	1.337,
        R: 24.000,
    },

    {
        pilha: "Pilha Alcalina Duracel AAA",
        tensao_nominal: 1.5,
        corrente: 1.2,
        E: 0.994,
        V: 0.851,
        R: 24.000,
    },

    {
        pilha: "Philips AAA ",
        tensao_nominal: 1.5,
        corrente: 1.2,
        E: 1.379,
        V: 1.331,
        R: 24.000,
    },

    {
        pilha: "Unipower",
        tensao_nominal: 12,
        corrente: 7000,
        E: 10.911,
        V: 10.116,
        R: 24.000,
    },

    {
        pilha: "Freedom",
        tensao_nominal: 12,
        corrente: 30000,
        E: 10.718,
        V: 	10.610,
        R: 24.000,
    },
];
 
// Função para calcular a resistencia interna de cada medição e preencher a tabela
function calcularResistencia() {
    var tabela = document.getElementById("tbDados");
   
    // Limpar tabela antes de preencher
    var tabelaHTML = `<tr>
                              <th>Pilha/Bateria</th>
                              <th>Tensão nominal (V)</th>
                              <th>Capacidade de corrente (mA.h)</th>
                              <th>Tensão sem carga(V)</th>
                              <th>Tensão com carga(V)</th>
                              <th>Resitência de carga(ohm)</th>
                              <th>Resistência interna(ohm)</th>
                    </tr>`;
   
    // Iterar sobre os dados e calcular o r de cada um
    for (var i = 0; i < dados.length; i++) {
      var linha = dados[i];
      var E = linha.E;
      var V = linha.V;
      var R = linha.R;
      var r = R * (E / V - 1);
   
      // Adicionar nova linha à tabela com os valores e a soma
      var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";
   
      tabelaHTML += novaLinha;
    }
    tabela.innerHTML = tabelaHTML;
  }
   
  calcularResistencia();