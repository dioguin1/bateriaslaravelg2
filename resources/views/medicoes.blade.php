@extends('templates.base')

@section('conteudo')

    <main>
        <h1>Medições</h1>
        <hr>
        <h2>Valores obtidos:</h2>
        <table class="table table-striped table-bordered" id="tbDados">
            <thead>
                <tr>
                    <th> Bateria </th>
                    <th> Tensão nominal </th>
                    <th> Capacidade de corrente </th>
                    <th> Tensão sem carga </th>
                    <th> Tensão com carga </th>
                    <th> Resistência interna "OHM"</th>
                    <th> Resistência de carga "OHM"</th>
                    
        
                </tr>
            </thead>
            <tbody>
                @foreach($medicoes as $medicao)
                <tr>
                    <td>{{$medicao->pilha_bateria}}</td>
                    <td>{{number_format($medicao->tensao_nominal,1,'.','')}}</td>
                    <td>{{$medicao->capacidade_corrente}}</td>
                    <td>{{$medicao->tensao_sem_carga}}</td>
                    <td>{{$medicao->tensao_com_carga}}</td>
                    <td>{{number_format($medicao->resistencia_interna,3,'.','')}}</td>
                    <td>{{$medicao->resistencia_interna,3,'.',''}}</td>
            </tr>
        @endforeach     
        </table>
    </main>

    @endsection

    @section('rodape')
        <h4>Rodapé de medições</h4>
    @endsection
    
