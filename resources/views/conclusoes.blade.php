@extends ('templates.base')

@section ('conteudo')
    <main>
        <h1>CONCLUSÕES</h1>
        <hr>
       
        <p>
        A conclusão feita, foi que, "a vida útil" de cada pilha ou bateria está associado com a tensão com carga relativamente à tensão nominal, assim como, o valor da respectiva resistência interna.
        Com tudo, aprendemos a calcular a resistência interna de uma pilha/bateria, utilizando dados calculados por um multímetro e associando às determiadas tensões de cada um. 
        Para o desenvolvimento do trabalho, tivemos que aplicar conhecimentos relacionados à HTML para a criação do site, em Eletroeletrônica para as análises no Multímetro e os cálculos realizados para saber resistência, tensão e etc...<br>
        À partir de um trabalho sobre baterias em geral, cada mebro em específico teve uma função condizente com sua preferência. Tais como, Bernardo, que manteve a parte de javascript atualizada corretamente, e que inclusive foi o “owner” de todo o projeto, nos guiando dentre todos os tipos de códigos que precisamos utilizar.
Diogo, que foi o gerente, se manteve centrado em fazer tudo que a equipe necessitava, contando também com a ajuda de todos os membros.
Bernardo, que participou principalmente nas feições do codigo HTML.
E por último, mas não menos importante, o Luiz, que participou da criação das rotas e contoler.


        </p>
    </main>
@endsection

@section('rodape')
<h4>Rodape conclusoes</h4>
@endsection